package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbXmlSaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJaxbXmlSaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String description() {
        return "Save current application state in xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxbXmlSaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.saveJaxbXml(new DataJaxbXmlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-jaxb-xml";
    }

}
