package ru.tsc.bagrintsev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class ExitListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Close program.";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        System.exit(0);
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String shortName() {
        return "";
    }

}
