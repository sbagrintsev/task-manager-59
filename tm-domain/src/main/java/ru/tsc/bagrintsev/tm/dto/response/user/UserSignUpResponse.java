package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;

@NoArgsConstructor
public final class UserSignUpResponse extends AbstractUserResponse {

    public UserSignUpResponse(@Nullable final UserDto user) {
        super(user);
    }

}
