package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @Override
    void add(@NotNull final TaskDto record);

    @Override
    void addAll(@NotNull final Collection<TaskDto> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<TaskDto> findAll();

    @Nullable
    List<TaskDto> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @Override
    @Nullable
    List<TaskDto> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    List<TaskDto> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Override
    @Nullable
    TaskDto findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final TaskDto task);

    @Override
    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
