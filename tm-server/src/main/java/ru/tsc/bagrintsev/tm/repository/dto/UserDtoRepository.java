package ru.tsc.bagrintsev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.bagrintsev.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.util.List;

@Repository
@Scope("prototype")
public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    public UserDtoRepository() {
        super(UserDto.class);
    }

    @Override
    public @Nullable UserDto findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        @NotNull final List<UserDto> result = entityManager
                .createQuery(jpql, clazz)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public @Nullable UserDto findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        @NotNull final List<UserDto> result = entityManager
                .createQuery(jpql, clazz)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public boolean isEmailExists(@NotNull String email) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExists(@NotNull String login) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.remove(findByLogin(login));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setParameter(@NotNull final UserDto user) {
        entityManager.merge(user);
    }

    @Override
    public void setRole(
            @NotNull final String login,
            @NotNull final Role role
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.role = :role " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("role", role)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.passwordHash = :password, " +
                        "m.passwordSalt = :salt " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("login", login)
                .setParameter("password", password)
                .setParameter("salt", salt)
                .executeUpdate();
    }

}
