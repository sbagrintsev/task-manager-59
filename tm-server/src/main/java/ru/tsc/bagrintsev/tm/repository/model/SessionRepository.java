package ru.tsc.bagrintsev.tm.repository.model;

import javax.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.model.ISessionRepository;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Session;

@Repository
@Scope("prototype")
public class SessionRepository extends UserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository() {
        super(Session.class);
    }

}
