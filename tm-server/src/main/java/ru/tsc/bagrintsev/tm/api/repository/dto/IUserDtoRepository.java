package ru.tsc.bagrintsev.tm.api.repository.dto;

import org.apache.ibatis.annotations.Mapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDto;
import ru.tsc.bagrintsev.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

@Mapper
public interface IUserDtoRepository extends IAbstractDtoRepository<UserDto> {

    @Override
    void add(@NotNull final UserDto model);

    @Override
    void addAll(@NotNull final Collection<UserDto> records);

    @Override
    void clearAll();

    @Override
    @Nullable
    List<UserDto> findAll();

    @Nullable
    UserDto findByEmail(@NotNull final String email);

    @Nullable
    UserDto findByLogin(@NotNull final String login);

    @Override
    @Nullable
    UserDto findOneById(@NotNull final String id);

    boolean isEmailExists(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    void removeByLogin(@NotNull final String login);

    void setParameter(@NotNull final UserDto user);

    void setRole(
            @NotNull final String login,
            @NotNull final Role role
    );

    void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    );

    @Override
    long totalCount();

}
