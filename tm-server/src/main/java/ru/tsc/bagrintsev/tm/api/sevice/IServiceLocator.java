package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.dto.*;

public interface IServiceLocator {

    @NotNull
    IAuthDtoService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

}
