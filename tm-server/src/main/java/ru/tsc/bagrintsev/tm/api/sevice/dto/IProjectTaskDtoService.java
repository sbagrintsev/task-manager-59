package ru.tsc.bagrintsev.tm.api.sevice.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

public interface IProjectTaskDtoService {

    TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException;

    TaskDto unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, TaskNotFoundException;

}
