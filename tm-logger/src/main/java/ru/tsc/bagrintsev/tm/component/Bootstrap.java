package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.IReceiverService;

import javax.jms.MessageListener;

@Getter
@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IReceiverService receiverService;

    @NotNull
    private final MessageListener loggerListener;

    public void run() {
        receiverService.receive(loggerListener);
    }

}
